totalpaid=0
count=1
previousbalance=balance
while count<=12:
    monthlypayment=round(previousbalance*monthlyPaymentRate,2)
    interest=(previousbalance-monthlypayment)*(annualInterestRate/12)
    previousbalance=round(previousbalance+interest-monthlypayment,2)
    print("Month:"+str(count))
    print("Minimum monthly payment:"+str(monthlypayment))
    print("Remaining balance:"+str(previousbalance))
    totalpaid=totalpaid+monthlypayment
    count=count+1
print("Total paid:"+str(totalpaid))
print("Remaining balance:"+str(previousbalance))
