# 6.00x Problem Set 3
#
# Successive Approximation: Newton's Method
#


# Problem 1: Polynomials
def evaluatePoly(poly, x):
    '''
    Computes the value of a polynomial function at given value x. Returns that
    value as a float.
 
    poly: list of numbers, length > 0
    x: number
    returns: float
    '''
    result=0
    count=len(poly)
    while count>0:
        result=result+(poly[(len(poly)-count)]*(x**(len(poly)-count)))
        count=count-1
        
    return float(result)

# Problem 2: Derivatives
def computeDeriv(poly):
    '''
    Computes and returns the derivative of a polynomial function as a list of
    floats. If the derivative is 0, returns [0.0].
 
    poly: list of numbers, length &gt; 0
    returns: list of numbers (floats)
    '''
    result=[]
    cou=0
    count=len(poly)-1
    if len(poly)==1:
            return [0.0]
    while count>0:
        
        result.append(float((cou+1)*(poly[cou+1])))
        count=count-1
        cou=cou+1
        
    return result

# Problem 3: Newton's Method
def computeRoot(poly, x_0, epsilon):
    '''
    Uses Newton's method to find and return a root of a polynomial function.
    Returns a list containing the root and the number of iterations required
    to get to the root.
 
    poly: list of numbers, length > 1.
         Represents a polynomial function containing at least one real root.
         The derivative of this polynomial function at x_0 is not 0.
    x_0: float
    epsilon: float > 0
    returns: list [float, int]
    '''
    x_1=x_0
    it=0
    while True:
        if abs(evaluatePoly(poly,x_1))<=epsilon:
            return [x_1,it]
            break
        else:
            x_2=x_1-((evaluatePoly(poly,x_1)/evaluatePoly(computeDeriv(poly),x_1)))
            x_1=x_2
            it=it+1
        
    
