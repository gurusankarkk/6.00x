# 6.00 Problem Set 3
# 
# Hangman game
#

# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)

import random
import string

WORDLIST_FILENAME = "words.txt"

def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print "Loading word list from file..."
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r', 0)
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = string.split(line)
    print "  ", len(wordlist), "words loaded."
    return wordlist

def chooseWord(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code
# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = loadWords()

def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE...
    count=0
    for letter in secretWord:
        for let in lettersGuessed:
            if let==letter:
                count=count+1
                break

    if count==len(secretWord):
        return True
    else:
        return False

def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    out=''
    # FILL IN YOUR CODE HERE...
    for letter in secretWord:
        count=0
        for let in lettersGuessed:
            
            if let==letter:
                out=out+let
                count=count+1
                break
        if count==0:
            out=out+'_'

    return out
            



def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE...
    import string
    out=''
    for letter in string.ascii_lowercase:
        count=0
        for char in lettersGuessed:
            if char==letter:
                count=1
                break
        if count==0:
            out=out+letter
    return out
            
                

def AlreadyGuessed(secretWord,lettersGuessed,guessInLowerCase):
    for ch in lettersGuessed:
        if ch==guessInLowerCase:
            return True
    return False     

def hangman(secretWord):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many 
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each round, you should also display to the user the 
      partially guessed word so far, as well as letters that the 
      user has not yet guessed.


    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE...
    print 'Welcome to the game, Hangman!'
    print 'I am thinking of a word that is ' +str(len(secretWord)) +' letters long.'
    count=8
    guessInLowerCase=[]
    lettersGuessed=[]
    print '-------------'
    cou=0
    while count>0:
        print 'You have '+ str(count)+  ' guesses left.'
        print 'Available letters: '+getAvailableLetters(lettersGuessed)
        guess=raw_input('Please guess a letter: ')
        guessInLowerCase = guess.lower()        
        if AlreadyGuessed(secretWord,lettersGuessed,guessInLowerCase)==False:
            lettersGuessed.append(guessInLowerCase)
            for i in secretWord:
                if i==guessInLowerCase:
                    co=0
                    break
                else:
                    co=1
            if co==0:
                print 'Good guess: '+getGuessedWord(secretWord, lettersGuessed)
                print '-------------'
            else:
                print 'Oops! That letter is not in my word: '+getGuessedWord(secretWord, lettersGuessed)
                print '-------------'
                count=count-1
        
                    
        elif AlreadyGuessed(secretWord,lettersGuessed,guessInLowerCase)==True:
             print 'Oops! You''ve already guessed that letter: '+getGuessedWord(secretWord, lettersGuessed)
             print '-------------'

        if isWordGuessed(secretWord, lettersGuessed):
            print 'Congratulations, you won!'
            break
        if count==0 & isWordGuessed(secretWord, lettersGuessed)==False:
            print 'Sorry, you ran out of guesses. The word was '+secretWord+'.'
    
# When you've completed your hangman function, uncomment these two lines
# and run this file to test! (hint: you might want to pick your own
# secretWord while you're testing)

secretWord = chooseWord(wordlist).lower()
hangman(secretWord)
